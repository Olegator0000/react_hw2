import React, { useState } from 'react';
import './ModalAdd.scss';
import PropTypes from 'prop-types';

const ModalAdd = ({ image, onClose, onAddToFavorites }) => {
    const [isFavorite, setIsFavorite] = useState(false);

    const handleAddToFavorites = () => {
        onAddToFavorites(image);
        setIsFavorite(true);
        onClose();
    };

    return (
        <div className="modal-background">
            <div className="modal-content">
                <h2>Add to Favorites</h2>
                <img src={image.imagePath} alt={image.alt} />
                <p>Do you want to add this image to your favorites?</p>
                <button
                    onClick={handleAddToFavorites}
                    style={{ backgroundColor: isFavorite ? 'red' : 'initial' }}
                >
                    Favorite Button
                </button>
                <button onClick={onClose}>Cancel</button>
            </div>
        </div>
    );
};

ModalAdd.propTypes = {
    image: PropTypes.shape({
        name: PropTypes.string,
        price: PropTypes.string,
        imagePath: PropTypes.string,
        sku: PropTypes.string,
        color: PropTypes.string
    }),
    onClose: PropTypes.func.isRequired,
    onAddToFavorites: PropTypes.func.isRequired
}

export default ModalAdd;
