import React, { useState, useEffect } from 'react';
import Button from './components/button/Button';
import GridImg from './components/GridItem/GridImg';
import './components/GridItem/Grid.scss';
import ModalAdd from './components/modal/ModalAdd';
import Header from './components/header/header.jsx';

function App() {
    const [isPressed, setIsPressed] = useState(false);
    const [items, setItems] = useState([]);
    const [favorite, setFavorite] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [selectedImage, setSelectedImage] = useState(null);

    const addToFavorite = (image) => {
        image.isFavorite = true;
        setFavorite((prevFavorite) => [...prevFavorite, image]);
    };

    const handleFavoriteButtonClick = (image) => {
        setSelectedImage(image);
        setShowModal(true);
    };

    useEffect(() => {
        if (isPressed) {
            fetch("./public/mock.json")
                .then(res => res.json())
                .then((result) => {
                    setItems(result.map(image => ({ ...image, isFavorite: favorite.some(fav => fav.id === image.id) })));
                });
        }
    }, [isPressed, favorite]);

    const handleLoadCars = () => {
        setIsPressed(true);
    };

    return (
        <div className="app">
            <Header favoriteCount={favorite.length} />
            <Button onClick={handleLoadCars}> Load Cars </Button>
            <GridImg items={items} handleFavoriteButtonClick={handleFavoriteButtonClick} favorites={favorite} />
            {showModal && (
                <ModalAdd
                    image={selectedImage}
                    onClose={() => setShowModal(false)}
                    onAddToFavorites={() => {
                        addToFavorite(selectedImage);
                        setShowModal(false);
                    }}
                />
            )}
        </div>
    );
}

export default App;
